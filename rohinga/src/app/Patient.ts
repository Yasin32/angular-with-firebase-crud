/**
 * Created by ya-seen on 9/16/18.
 */
export class Patient{
  name: string;
  age: number;
  fatherName: string;
  block: string;
  sex: string;
  date: Date;
  caregiver: string;
}
