import { Component, OnInit } from '@angular/core';
import {Patient} from "../Patient";
import {FireBaseService} from "../fire-base.service";

@Component({
  selector: 'app-create-patient',
  templateUrl: './create-patient.component.html',
  styleUrls: ['./create-patient.component.css']
})
export class CreatePatientComponent implements OnInit {
  patient = {
    name: '',
    age: null,
    fatherName: '',
    block: '',
    date: null,
    careGiver: '',
    sex: ''
  };

  constructor(private fireBase: FireBaseService) { }

  ngOnInit() {
  }

  createPatient() {
    this.fireBase.createPatient(this.patient);
      // .subscribe(response=>console.log(response));
  }

}
