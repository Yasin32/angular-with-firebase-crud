import { Component, OnInit } from '@angular/core';
import {Patient} from "../Patient";
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  patients: any;
  patientRef: any;

  constructor(private db: AngularFireDatabase) {
    this.patientRef = db.list('/patients/');
  }

  ngOnInit() {
    this.getList();
  }

  getList() {
    this.patientRef.valueChanges().subscribe( (response) => {
      this.patients = response;
    });
  }

}
