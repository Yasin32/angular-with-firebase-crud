import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable({
  providedIn: 'root'
})
export class FireBaseService {

  patientRef: any;

  constructor(private db: AngularFireDatabase) {
    this.patientRef = db.list('/patients/')
  }

  createPatient(data: any) {
    this.patientRef.push(data);
  }
}
